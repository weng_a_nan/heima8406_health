package com.itheima.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constant.MessageConstant;
import com.itheima.entity.Result;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderSettingService;
import com.itheima.utils.DateTimeUtil;
import com.itheima.utils.POIUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ordersetting")
public class OrderSettingController {
    @Reference
    private OrderSettingService orderSettingService;

    @RequestMapping("/upload")
    public Result upload(@RequestParam("excelFile") MultipartFile excelFile) {
        try {
            //读取上传的excel
            List<String[]> list = POIUtils.readExcel(excelFile);
            //将文件中的内容装换成OrderSetting
            List<OrderSetting> orderSettings = new ArrayList<OrderSetting>(list.size());
            for (String[] strings : list) {
                OrderSetting orderSetting = new OrderSetting();
                orderSetting.setOrderDate(DateTimeUtil.getDateByString(strings[0], "yyyy/MM/dd"));
                orderSetting.setNumber(Integer.parseInt(strings[1]));
                orderSettings.add(orderSetting);
            }
            //调用orderSettingService中的批量导入方法
            orderSettingService.batchInsert(orderSettings);
            return new Result(true, MessageConstant.IMPORT_ORDERSETTING_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.IMPORT_ORDERSETTING_FAIL);
        }
    }

    /**
     * @param date
     * @return
     */
    @RequestMapping("/getOrderSettingByMonth")
    public Result getOrderSettingByMonth(String date) {
        try {
            List<OrderSetting> settings = orderSettingService.getOrderSettingByMonth(date);
            //数据格式的转换 { date: 1, number: 120, reservations: 1 }
            List<Map<String, Integer>> results = new ArrayList<>(settings.size());
            for (OrderSetting setting : settings) {
                Map<String, Integer> resultMap = new HashMap<String, Integer>(3);
                resultMap.put("date", setting.getOrderDate().getDate());
                resultMap.put("number", setting.getNumber());
                resultMap.put("reservations", setting.getReservations());
                results.add(resultMap);
            }
            return new Result(true, MessageConstant.QUERY_ORDER_SETTING_SUCCESS, results);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_ORDER_SETTING_FAIL);
        }
    }

    /**
     * @param date   2020-02-02
     * @param number
     * @return
     */
    @RequestMapping("/editNumberByDate")
    public Result editNumberByDate(String date, Integer number) {
        try {
            OrderSetting orderSetting = new OrderSetting(DateTimeUtil.getDateByString(date, "yyyy-MM-dd"), number);
            orderSettingService.editNumberByDate(orderSetting);
            return new Result(true, MessageConstant.EDIT_ORDER_SETTING_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_ORDER_SETTING_FAIL);
        }
    }

    @RequestMapping("/editNumberByDate1")
    public Result editNumberByDate1(@RequestBody OrderSetting orderSetting) {
        try {
            orderSettingService.editNumberByDate(orderSetting);
            return new Result(true, MessageConstant.EDIT_ORDER_SETTING_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_ORDER_SETTING_FAIL);
        }
    }
}
