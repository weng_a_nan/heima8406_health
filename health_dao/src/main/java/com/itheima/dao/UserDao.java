package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.User;

import java.util.List;
import java.util.Map;

public interface UserDao {
    /**
     * 根据username查询出用户基本信息
     * @param username
     * @return
     */
    User findByUsername(String username);

    Page<User> findPage(String queryString);

    void add(User user);

    void setUserAndRole(Map<String, Integer> map);

    User findById(Integer id);

    void edit(User user);

    void deleteAssociation(Integer id);

    List<Integer> findRoleIdsByUserId(Integer id);

    void delete(Integer id);
}
