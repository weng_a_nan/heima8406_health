package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface RoleDao {
    /**
     * 根据用户id查询该用户的角色信息
     * @param id
     * @return
     */

    Set<Role> findByUserId(Integer id);


    void roleAdd(Role role);

    void roleMenu(@Param("roleId") Integer roleId,@Param("menuId") Integer menuId);

    void rolePermission(@Param("roleId") Integer roleId,@Param("permissionId") Integer permissionId);

    Page<Role> findAll(String queryString);

    Role roleUpdateOne(Integer id);

    List<Integer> roleUpdateTwo(Integer id);

    List<Integer> roleUpdateThree(Integer id);

    void roleEdit(Role role);

    void roleDeleteMenu(Integer id);

    void roleDeletePermission(Integer id);

    Integer findCountRoleMenu();

    Integer findCountRoleUser();

    Integer findCountRolePermission();

    void roleDelete(Integer id);



    List<Role> findAll4User();
}
