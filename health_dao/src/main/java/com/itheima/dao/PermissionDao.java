package com.itheima.dao;

import com.itheima.pojo.Permission;

import java.util.List;
import java.util.Set;

public interface PermissionDao {
    /**
     * 按照角色id查询权限信息
     * @param id
     * @return
     */
    Set<Permission> findByRoleId(Integer id);

    List<Permission> findAll(String str);

    void permissionAdd(Permission permission);

    Permission permissionUpdate(Integer id);

    void permissionEdit(Permission permission);

    void permissionDelete(Integer id);

    List<Permission> findAllPermission();
}
