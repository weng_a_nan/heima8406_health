package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetmealDao {
    /**
     * 保存套餐基本信息
     * @param setmeal
     */
    void add(Setmeal setmeal);

    /**
     * 保存检查组和套餐的关联数据
     * @param map
     */
    void setSetmealAndCheckGroup(Map<String, Integer> map);

    /**
     * 分页
     * @param queryString
     * @return
     */
    Page<Setmeal> selectByQueryString(String queryString);

    /**
     * 查询全部套餐
     * @return
     */
    List<Setmeal> findAll();

    /**
     * 主键查询
     * @param id
     * @return
     */
    Setmeal findById(Integer id);

    /**
     * 查询套餐预约情况
     * @return
     */
    List<Map<String, Object>> findSetmealCounts();

    /**
     * 获取热门套餐
     * @return
     */
    List<Map<String, Object>> findHotSetmeal();
}
