package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckGroup;

import java.util.List;
import java.util.Map;

public interface CheckGroupDao {
    /**
     * 保存检查组基本信息
     * @param checkGroup
     */
    void add(CheckGroup checkGroup);

    /**
     * 保存检查组和检查项的关联信息
     * @param map
     */
    void setCheckGroupAndCheckItem(Map<String, Integer> map);

    /**
     * 检查组分页
     * @param queryString
     * @return
     */
    Page<CheckGroup> selectByCondition(String queryString);

    /**
     * 主键查询
     * @param id
     * @return
     */
    CheckGroup findById(Integer id);

    /**
     * 根据检查组id获取关联的检查项id
     * @param id
     * @return
     */
    List<Integer> findCheckItemIdsByCheckGroupId(Integer id);

    /**
     * 更新检查组基本信息
     * @param checkGroup
     */
    void edit(CheckGroup checkGroup);

    /**
     * 依据检查组id删除旧的检查组和检查项的关联数据
     * @param id
     */
    void deleteAssociation(Integer id);

    /**
     * 校验检查组是否被套餐给引用(t_setmeal_checkgroup)
     * @param id
     * @return
     */
    Integer getCount4Association(Integer id);

    /**
     * 删除检查组
     * @param id
     */
    void delete(Integer id);

    /**
     * 查询全部检查组
     * @return
     */
    List<CheckGroup> findAll();

    /**
     *
     * @param id
     * @return
     */
    List<CheckGroup> findBySetmealId(Integer id);
}
