package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckItem;

import java.util.List;

public interface CheckItemDao {
    /**
     * 新增检查项
     * @param checkItem
     */
    void add(CheckItem checkItem);

    /**
     * 分页查询
     * @param queryString
     * @return
     */
    Page<CheckItem> findByCodeOrName(String queryString);

    /**
     * 校验检查项是否被检查组使用
     * @return
     */
    Integer findCountByCheckItemId(int id);

    /**
     * 删除检查项
     * @param id
     */
    void delete(Integer id);

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    CheckItem findById(Integer id);

    /**
     * 更新检查项
     * @param checkItem
     */
    void edit(CheckItem checkItem);

    /**
     * 查询全部检查项
     * @return
     */
    List<CheckItem> findAll();

    List<CheckItem> findByCheckGroupId(Integer id);
}
