package com.itheima.dao;

import com.itheima.pojo.Member;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface MemberDao {
    /**
     * 根据手机号码查询会员
     *
     * @param telphone
     * @return
     */
    Member findByTel(String telphone);

    /**
     * 新增会员
     *
     * @param member
     */
    void add(Member member);

    /**
     * 按照月份获取会员总数（会员注册时间小于或等于对应月份的最后一天）
     *
     * @param month yyyy-MM-dd
     * @return
     */
    Integer findCountByMonth(String month);

    /**
     * 按照日期获取当天新增的会员数
     * @param reportDate
     * @return
     */
    Long findCountByEqualDate(String reportDate);

    /**
     * 获取会员总数
     * @return
     */
    Long findAllCount();

    /**
     * 按照日期获取大于等于某一天的会员数
     * @param reportDate
     * @return
     */
    Long findCountByGreaterAndEqualDate(String reportDate);

    List<Map> getCountsBySex();

    int findCountByAge(Map<String, String> ageMap);

    Integer reportAgeAndSexman();

    Integer reportAgeAndSexwoman();

    Integer AgeBandFunction(@Param("start") Date start, @Param("stop") Date stop);

    Integer AgeBandFunctionNull(Date start);
}
