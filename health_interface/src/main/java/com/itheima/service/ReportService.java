package com.itheima.service;

import com.alibaba.dubbo.config.annotation.Service;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface ReportService {
    /**
     * 获取运营统计数据
     *
     * @return
     */
    Map<String, Object> getBusinessReport() throws Exception;

    List<Map<String,Object>> reportAgeAndSex();


    List<Map<String,Object>> AgeBand() throws ParseException;
}
