package com.itheima.service;

import java.util.Map;

public interface OrderService {
    /**
     * 提交预约
     * @param map
     */
    Integer submit(Map map) throws Exception;

    /**
     * 根据id查询预约信息
     * @param id
     * @return
     */
    Map findById(Integer id);
}
