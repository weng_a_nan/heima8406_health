package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Permission;

import java.util.List;

public interface PermissionService {
    PageResult findAll(QueryPageBean queryPageBean);

    void permissionAdd(Permission permission);

    Permission permissionUpdate(Integer id);

    void permissionEdit(Permission permission);

    void permissionDelete(Integer id);
}
