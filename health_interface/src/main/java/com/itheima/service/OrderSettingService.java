package com.itheima.service;

import com.itheima.pojo.OrderSetting;

import java.util.List;

public interface OrderSettingService {
    /**
     * 批量导入预约数据
     * @param orderSettings
     */
    void batchInsert(List<OrderSetting> orderSettings);

    /**
     * 按照月份查询预约设置信息
     * @param date
     * @return
     */
    List<OrderSetting> getOrderSettingByMonth(String date);

    /**
     * 设置预约信息
     * @param orderSetting
     */
    void editNumberByDate(OrderSetting orderSetting);
}
