package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckItem;

import java.util.List;

public interface CheckItemService {
    /**
     * 保存检查项
     *
     * @param checkItem
     */
    void add(CheckItem checkItem);

    /**
     * 检查项分页
     *
     * @param queryPageBean
     * @return
     */
    PageResult findPage(QueryPageBean queryPageBean);

    /**
     * 删除检查项
     *
     * @param id
     */
    void delete(Integer id) throws Exception;

    /**
     * 根据主键查询
     * @param id
     * @return
     */
    CheckItem findById(Integer id);

    /**
     * 更新检查项
     * @param checkItem
     */
    void edit(CheckItem checkItem);

    /**
     * 查询全部检查项
     * @return
     */
    List<CheckItem> findAll();
}
