package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Setmeal;

import java.util.List;
import java.util.Map;

public interface SetmealService {
    /**
     * 保存套餐
     * @param checkgroupIds
     * @param setmeal
     */
    void add(Integer[] checkgroupIds, Setmeal setmeal);

    /**
     * 套餐分页
     * @param queryPageBean
     * @return
     */
    PageResult findPage(QueryPageBean queryPageBean);

    /**
     * 查询全部套餐
     * @return
     */
    List<Setmeal> findAll();

    /**
     * 主键查询
     * @param id
     * @return
     */
    Setmeal findById(Integer id);

    /**
     * 查询套餐预约情况
     * @return
     */
    List<Map<String, Object>> findSetmealCounts();
}
