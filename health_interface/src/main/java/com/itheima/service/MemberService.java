package com.itheima.service;

import com.itheima.pojo.Member;

import java.util.List;
import java.util.Map;

public interface MemberService {
    /**
     * 根据手机号码查询会员信息
     * @param telephone
     * @return
     */
    Member findByTel(String telephone);

    /**
     * 保存会员
     * @param member
     */
    void add(Member member);

    /**
     * 获取对应月份的会员数量
     * @param months
     * @return
     */
    List<Integer> findCountByMonths(List<String> months);

    List<Map> getCountsBySex();

    int findCountByAge(Map<String, String> ageMap);
}
