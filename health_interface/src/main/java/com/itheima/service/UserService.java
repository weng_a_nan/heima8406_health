package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.User;

import java.util.List;

public interface UserService {
    /**
     * 按照用户名查询出用户基本信息、角色信息和权限信息
     * @param username
     * @return
     */
    User findByUsername(String username);

    PageResult findPage(QueryPageBean queryPageBean);

    void add(User user, Integer[] roleIds);


    User findById(Integer id);

    void edit(User user, Integer[] roleIds);

    List<Integer> findRoleIdsByUserId(Integer id);

    void delete(Integer id);
}
