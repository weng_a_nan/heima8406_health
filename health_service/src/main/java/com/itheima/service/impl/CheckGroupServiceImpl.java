package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckGroupDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckGroup;
import com.itheima.service.CheckGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = CheckGroupService.class)
@Transactional
public class CheckGroupServiceImpl implements CheckGroupService {

    @Autowired
    private CheckGroupDao checkGroupDao;

    /**
     * 新增检查组
     *
     * @param checkGroup
     * @param checkitemIds
     */
    @Override
    public void add(CheckGroup checkGroup, Integer[] checkitemIds) {
        //保存检查组基本信息,获取自增的主键值
        checkGroupDao.add(checkGroup);
        //保存检查组和检查项的关联信息(t_checkgroup_checkitem)
        int checkgroupId = checkGroup.getId();
        saveCheckGroupAndCheckitemAssociation(checkitemIds, checkgroupId);
    }

    /**
     * 检查组分页
     *
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        Page<CheckGroup> page = checkGroupDao.selectByCondition(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), page.getResult());
    }

    /**
     * 主键查询
     *
     * @param id
     * @return
     */
    @Override
    public CheckGroup findById(Integer id) {
        return checkGroupDao.findById(id);
    }

    /**
     * 根据检查组id获取关联的检查项id
     *
     * @param id
     * @return
     */
    @Override
    public List<Integer> findCheckItemIdsByCheckGroupId(Integer id) {
        return checkGroupDao.findCheckItemIdsByCheckGroupId(id);
    }

    /**
     * 更新检查组
     *
     * @param checkGroup
     * @param checkitemIds
     */
    @Override
    public void edit(CheckGroup checkGroup, Integer[] checkitemIds) {
        //1 更新检查组的基本信息
        checkGroupDao.edit(checkGroup);
        //2 依据检查组id删除该检查组关联旧的检查项id(t_checkgroup_checkitem)
        checkGroupDao.deleteAssociation(checkGroup.getId());
        //3 保存新的检查组和检查项的关联数据(t_checkgroup_checkitem)
        int checkgroupId = checkGroup.getId();
        saveCheckGroupAndCheckitemAssociation(checkitemIds, checkgroupId);
    }

    /**
     * 删除检查组
     *
     * @param id
     */
    @Override
    public void delete(Integer id) throws Exception{
        //1 校验要删除的检查组是否被套餐给使用了(t_setmeal_checkgroup)
        Integer count = checkGroupDao.getCount4Association(id);
        //1.1 被使用则无法删除
        if(count > 0){
            throw new Exception("要删除的检查组被使用,无法删除");
        }
        //1.2 未被使用,则执行步骤2
        //2 删除检查组和检查项的关联表(t_checkgroup_checkitem)的数据,根据检查组id进行删除
        checkGroupDao.deleteAssociation(id);
        //3 删除检查组
        checkGroupDao.delete(id);
    }

    /**
     * 查询全部检查组
     *
     * @return
     */
    @Override
    public List<CheckGroup> findAll() {
        return checkGroupDao.findAll();
    }

    private void saveCheckGroupAndCheckitemAssociation(Integer[] checkitemIds, int checkgroupId) {
        for (Integer checkitemId : checkitemIds) {
            Map<String, Integer> map = new HashMap<String, Integer>(2);
            map.put("checkgroup_id", checkgroupId);
            map.put("checkitem_id", checkitemId);
            checkGroupDao.setCheckGroupAndCheckItem(map);
        }
    }
}
