package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.ClearOldDateDao;
import com.itheima.service.ClearOldDateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Service(interfaceClass = ClearOldDateService.class)
@Transactional
public class ClearOldDateServiceImpl implements ClearOldDateService {
    @Autowired
    private ClearOldDateDao clearOldDateDao;
    @Override
    public void deleteDate() {
        clearOldDateDao.deleteDate();
        System.out.printf("测试执行了Dao啊啊啊啊啊啊啊啊啊啊啊啊阿啊啊啊");
    }
}
