package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.MemberDao;
import com.itheima.pojo.Member;
import com.itheima.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service(interfaceClass = MemberService.class)
@Transactional
public class MemberServiceImpl implements MemberService {
    @Autowired
    private MemberDao memberDao;

    /**
     * 根据手机号码查询会员信息
     *
     * @param telephone
     * @return
     */
    @Override
    public Member findByTel(String telephone) {
        return memberDao.findByTel(telephone);
    }

    /**
     * 保存会员
     *
     * @param member
     */
    @Override
    public void add(Member member) {
        memberDao.add(member);
    }

    @Override
    public List<Integer> findCountByMonths(List<String> months) {
        List<Integer> memberCounts = new ArrayList<Integer>(months.size());
        for (String month : months) {
            //month 2020-02
            //SELECT COUNT(*) FROM t_member WHERE regTime <= '2020-02-31'
            Integer count = memberDao.findCountByMonth(month + "-1");
            memberCounts.add(count);
        }
        return memberCounts;
    }

    @Override
    public List<Map> getCountsBySex() {
        return memberDao.getCountsBySex();
    }

    @Override
    public int findCountByAge(Map<String, String> ageMap) {
        return memberDao.findCountByAge(ageMap);
    }
}
