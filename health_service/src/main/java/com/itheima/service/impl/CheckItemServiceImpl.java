package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.CheckItemDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service(interfaceClass = CheckItemService.class)
@Transactional
public class CheckItemServiceImpl implements CheckItemService {
    @Autowired
    private CheckItemDao checkItemDao;

    /**
     * 保存检查项
     *
     * @param checkItem
     */
    @Override
    public void add(CheckItem checkItem) {
        //调用CheckItemDao中的保存方法
        checkItemDao.add(checkItem);
    }

    /**
     * 检查项分页
     *
     * @param queryPageBean
     * @return
     */
    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {
        //1 调用分页组件
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());//固定用法
        //2 调用CheckItemDao
        Page<CheckItem> page = checkItemDao.findByCodeOrName(queryPageBean.getQueryString());//Page<xxx>,固定写法,xxx,对应的查询sql要转换成的JavaBean
        //3 返回PageResult
        return new PageResult(page.getTotal(),page.getResult());
    }

    /**
     * 删除检查项
     *
     * @param id
     */
    @Override
    public void delete(Integer id) throws Exception{
        //2 校验要删除的检查项是否被检查组给使用
        Integer count = checkItemDao.findCountByCheckItemId(id);
        //2.1 若被使用,则无法删除
        if(count > 0){
            throw new Exception("检查项被使用,无法删除");
        }
        //2.2 未被使用,则可以删除(步骤3)
        //3 调用CheckItemDao中的删除方法
        checkItemDao.delete(id);
    }

    /**
     * 根据主键查询
     *
     * @param id
     * @return
     */
    @Override
    public CheckItem findById(Integer id) {
        return checkItemDao.findById(id);
    }

    /**
     * 更新检查项
     *
     * @param checkItem
     */
    @Override
    public void edit(CheckItem checkItem) {
        checkItemDao.edit(checkItem);
    }

    /**
     * 查询全部检查项
     *
     * @return
     */
    @Override
    public List<CheckItem> findAll() {
        return checkItemDao.findAll();
    }
}
