package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.MemberDao;
import com.itheima.dao.OrderDao;
import com.itheima.dao.SetmealDao;
import com.itheima.service.ReportService;
import com.itheima.utils.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Transactional
@Service(interfaceClass =ReportService.class )
public class ReportServiceImpl implements ReportService {
    @Autowired
    private MemberDao memberDao;
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private SetmealDao setmealDao;


    /**
     * @return
     */
    public Map<String, Object> getBusinessReport() throws Exception {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        //处理reportDate
        String reportDate = DateTimeUtil.getStringByDate(new Date(), "yyyy-MM-dd");
        resultMap.put("reportDate", reportDate);
        //本周第一天
        String reportDate4ThisWeek = DateTimeUtil.getStringByDate(DateTimeUtil.getDayOfWeek(new Date(), 1), "yyyy-MM-dd");
        //本月第一天
        String reportDate4ThisMonth = DateTimeUtil.getStringByDate(DateTimeUtil.firstDayOfMonth(new Date()), "yyyy-MM-dd");


        //处理todayNewMember
        Long todayNewMember = memberDao.findCountByEqualDate(reportDate);
        resultMap.put("todayNewMember", todayNewMember);
        // 处理totalMember
        Long totalMember = memberDao.findAllCount();
        resultMap.put("totalMember", totalMember);
        //处理thisWeekNewMember
        Long thisWeekNewMember = memberDao.findCountByGreaterAndEqualDate(reportDate4ThisWeek);
        resultMap.put("thisWeekNewMember", thisWeekNewMember);
        //处理thisMonthNewMember
        Long thisMonthNewMember = memberDao.findCountByGreaterAndEqualDate(reportDate4ThisMonth);
        resultMap.put("thisMonthNewMember", thisMonthNewMember);
        //处理todayOrderNumber
        Long todayOrderNumber = orderDao.findCountByEqualDate(reportDate);
        resultMap.put("todayOrderNumber", todayOrderNumber);
        //处理 todayVisitsNumber
        Long todayVisitsNumber = orderDao.findVisitedCountByEqualDate(reportDate);
        resultMap.put("todayVisitsNumber", todayVisitsNumber);
        //处理thisWeekOrderNumber
        Long thisWeekOrderNumber = orderDao.findCountByGreateAndEqualDate(reportDate4ThisWeek);
        resultMap.put("thisWeekOrderNumber", thisWeekOrderNumber);
        //处理thisWeekVisitsNumber
        Long thisWeekVisitsNumber = orderDao.findVisitedCountByGreaterAndEqualDate(reportDate4ThisWeek);
        resultMap.put("thisWeekVisitsNumber", thisWeekVisitsNumber);
        //处理thisMonthOrderNumber
        Long thisMonthOrderNumber = orderDao.findCountByGreateAndEqualDate(reportDate4ThisMonth);
        resultMap.put("thisMonthOrderNumber", thisMonthOrderNumber);
        //处理thisMonthVisitsNumber
        Long thisMonthVisitsNumber = orderDao.findVisitedCountByGreaterAndEqualDate(reportDate4ThisMonth);
        resultMap.put("thisMonthVisitsNumber", thisMonthVisitsNumber);
        //处理热门hotSetmeal
        List<Map<String, Object>> hotSetmeal = setmealDao.findHotSetmeal();
        resultMap.put("hotSetmeal", hotSetmeal);
        return resultMap;
    }

    @Override
    public List<Map<String, Object>> reportAgeAndSex() {
        List<Map<String, Object>> objects = new ArrayList<>();

        Map<String, Object> stringMan = new HashMap<>();
        Integer max =  memberDao.reportAgeAndSexman();
        stringMan.put("value",max);
        stringMan.put("name","男");
        objects.add(stringMan);

        Map<String, Object> stringWoman = new HashMap<>();
        Integer woman =  memberDao.reportAgeAndSexwoman();
        stringWoman.put("value",woman);
        stringWoman.put("name","女");
        objects.add(stringWoman);
        return objects;
    }

    @Override
    public List<Map<String, Object>> AgeBand() throws ParseException {
        List<Map<String, Object>> objects = new ArrayList<>();
        Map<String, Object> one = new HashMap<>();
        one.put("value",AgeBandFunction(0, 18));
        one.put("name","0-18");
        objects.add(one);

        Map<String, Object> tow = new HashMap<>();
        tow.put("value",AgeBandFunction(18, 30));
        tow.put("name","18-30");
        objects.add(tow);

        Map<String, Object> three = new HashMap<>();
        three.put("value",AgeBandFunction(30, 45));
        three.put("name","30-45");
        objects.add(three);

        Map<String, Object> may = new HashMap<>();
        may.put("value",AgeBandFunction(45, null));
        may.put("name","45以上");
        objects.add(may);
        return objects;



    }

    public Integer AgeBandFunction(Integer Intstart,Integer Intstop) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = simpleDateFormat.format(new Date());
        String substring = format.substring(4);//-02-21
        String newYear = format.substring(0,4);//2020
        Integer count =0;
        Date start = null;
        Date stop = null;


        start = simpleDateFormat.parse(Integer.toString(Integer.parseInt(newYear)-Intstart)+substring);
        if(Intstop != null){
         stop = simpleDateFormat.parse(Integer.toString(Integer.parseInt(newYear)-Intstop)+substring);
         count = memberDao.AgeBandFunction(start,stop);
        }else{
            count = memberDao.AgeBandFunctionNull(start);
        }
        return count;
    }

}
