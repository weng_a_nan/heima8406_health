package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.constant.MessageConstant;
import com.itheima.dao.MemberDao;
import com.itheima.dao.OrderDao;
import com.itheima.dao.OrderSettingDao;
import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderService;
import com.itheima.utils.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service(interfaceClass = OrderService.class)
@Transactional
public class OrderServiceImpl implements OrderService {
    @Autowired
    private MemberDao memberDao;
    @Autowired
    private OrderSettingDao orderSettingDao;
    @Autowired
    private OrderDao orderDao;

    /**
     * 提交预约
     *
     * @param map
     */
    @Override
    public Integer submit(Map map) throws Exception {
        Integer setmealId = Integer.parseInt(map.get("setmealId").toString());
        //1. 判断用户是否是会员
        String telphone = (String) map.get("telephone");
        Member member = memberDao.findByTel(telphone);
        //+ 非会员则向t_member表插入一条数据
        if (null == member) {
            member = new Member();
            member.setName(map.get("name").toString());
            member.setSex(map.get("sex").toString());
            member.setIdCard(map.get("idCard").toString());
            member.setPhoneNumber(map.get("telephone").toString());
            member.setRegTime(new Date());
            memberDao.add(member);
        }
        //2. 校验用户选择的日期是否可以预约(t_ordersetting)
        Date date = DateTimeUtil.getDateByString(map.get("orderDate").toString(), "yyyy-MM-dd");
        OrderSetting orderSetting = orderSettingDao.findByOrderDate(date);
        if (null == orderSetting) {
            //对应的日期是否设置过预约设置信息
            throw new Exception(MessageConstant.ORDER_FULL);
        }
        if (orderSetting.getNumber() <= orderSetting.getReservations()) {
            // + 对应的日期是否已经约满
            throw new Exception(MessageConstant.ORDER_FULL);
        }

        //3. 校验用户是否重复预约（同一个手机号在同一天内对同一个套餐）
        Order temp = new Order();
        temp.setMemberId(member.getId());
        temp.setOrderDate(date);
        temp.setSetmealId(setmealId);
        Order order = orderDao.findByMemberIdAndOrderDateAndSetmealId(temp);
        //  已预约过则无法预约
        if (order != null) {
            throw new Exception(MessageConstant.HAS_ORDERED);
        }

        //+ 向t_order表插入一条预约数据
        temp.setOrderType(map.get("orderType").toString());
        temp.setOrderStatus(map.get("orderStatus").toString());
        orderDao.add(temp);
        // 按照日期更新t_ordersetting表中的已预约人数
        Map<String, Object> paramMap = new HashMap<String, Object>(2);
        paramMap.put("date", date);
        paramMap.put("reservations", orderSetting.getReservations() + 1);
        orderSettingDao.updateReservationsByOrderDate(paramMap);
        return temp.getId();
    }

    /**
     * 根据id查询预约信息
     *
     * @param id
     * @return
     */
    @Override
    public Map findById(Integer id) {
        return orderDao.findById(id);
    }
}
