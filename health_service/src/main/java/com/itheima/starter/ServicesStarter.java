package com.itheima.starter;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class ServicesStarter {
    public static void main(String[] args) throws InterruptedException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext-service.xml");
        System.out.println("已加载");
        Thread.sleep(Long.MAX_VALUE);
    }
}
