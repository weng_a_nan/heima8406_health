package com.itheima.test;

import com.itheima.dao.MenuDao;
import com.itheima.pojo.Menu;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class MenuServiceTest {
    @Test
    public void findByUserTest(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext-dao.xml");
        MenuDao bean = context.getBean(MenuDao.class);
        int id = 2;
        List<Menu> menu = bean.findByRoleId(id);
        for (Menu menu1 : menu) {
            System.out.println(menu1.getName());
            for (Menu child : menu1.getChildren()) {
                System.out.println("    "+child.getName());
            }
        }
    }
}
