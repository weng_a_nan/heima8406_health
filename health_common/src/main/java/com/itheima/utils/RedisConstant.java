package com.itheima.utils;


public class RedisConstant {
    //所有上传到七牛云空间的图片名称
    public static final String SETMEAL_PIC_RESOURCES = "setmealPicResources";
    //保存在数据库中的图片名称
    public static final String SETMEAL_PIC_DB_RESOURCES = "setmealPicDbResources";
}
