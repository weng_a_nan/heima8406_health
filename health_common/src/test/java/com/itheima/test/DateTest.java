package com.itheima.test;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTest {

    @Test
    public void test() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = simpleDateFormat.format(new Date());
        //System.out.println(format);
        String substring = format.substring(4);//-02-21
        String newYear = format.substring(0,4);//2020

        Date start = simpleDateFormat.parse(Integer.toString(Integer.parseInt(newYear)-0)+substring);
        Date stop = simpleDateFormat.parse(Integer.toString(Integer.parseInt(newYear)-18)+substring);
        System.out.println(start);
        System.out.println(stop);
    }
}
