package com.itheima.jobs;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.service.ClearOldDateService;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClearOldDate {

    @Reference
    private ClearOldDateService clearOldDateService;
    public void ClearDateJob(){
        clearOldDateService.deleteDate();
    }
}
